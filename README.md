# @wxlfrank/downloader

[![npm](https://img.shields.io/npm/v/@wxlfrank/downloader?style=flat-square)](https://gitlab.com/wxlfrank/downloader)

## How to use

```typescript
import * as fs from 'fs';
import * as iconv from 'iconv-lite';
const filename = 'temp';
const host = 'http://xh.5156edu.com';
const sourceurl = host + '/pinyi.html';
const downloader = new Downloader(
  (id: number, url: string, data: string) => {
    console.log('result comes');
    fs.writeFile(filename, data, () => {
      console.log(filename + ' is finished writting');
    });
  },
  undefined,
  (rawData: Buffer) => {
    const str = iconv.decode(rawData, 'GB2312');
    return iconv.encode(str, 'utf8').toString();
  },
  [sourceurl],
);
downloader.run();
```

### with customized request

```typescript
const downloader = new Downloader(
  (index?: number, url?: string, r?: string) => {
    expect(r != null && r.length > 0).toBeTruthy();
  },
  undefined,
  undefined,
  ['a'],
  (http: any, index: number, url: string, callback: any) => {
    const data = 'wd=' + url;
    return {
      request: http.request(
        host,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(data),
          },
        },
        callback,
      ),
      data,
    };
  },
);
downloader.run();
```

## version updates log

### version 2.0.2

- add option to create customized reqeust for a given url

### version 2.0.3

- add test case for empty and post downloading
