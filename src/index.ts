import * as http from 'http';
interface RequestAndData {
  request: any;
  data: string;
}
type UrlGenerator = (i: any) => string | null;
type ResultHandler = (index?: number, url?: string, r?: string) => void;
type ResponseHandler = (r: Buffer) => string;
export type ErrorReport = { index: number; url: string; reason: string };
export type RequestGenerator = (
  http: any,
  index: number,
  url: string,
  callback: (res: http.IncomingMessage) => void,
) => RequestAndData;
export class Downloader {
  urlGenerator: UrlGenerator;
  urlSource: any[] | null;
  handleResult: ResultHandler;
  handleResponse: ResponseHandler;
  requestGenerator: RequestGenerator | null;
  MAX = 5;
  curIndex = 0;
  errorUrlCount: { [key: string]: number } = {};
  errorReport: ErrorReport[] = [];
  nextTry = 0;
  handleCount = 0;
  resolve: any;
  constructor(
    handler: ResultHandler,
    generator?: UrlGenerator,
    handleResponse?: ResponseHandler,
    source?: any[] | null,
    requestGenerator?: RequestGenerator | null,
  ) {
    this.urlSource = source || null;
    this.handleResult = handler;
    if (generator == null && source == null) {
      this.urlGenerator = (i: any) => null;
    } else {
      this.urlGenerator = generator || ((i: any) => (i == null ? null : i + ''));
    }
    this.handleResponse = handleResponse || ((v: Buffer) => v.toString());
    this.requestGenerator = requestGenerator || null;
  }
  getNextUrl(index: number): string | null {
    const next: any = this.urlSource != null ? this.urlSource[index] : index;
    return this.urlGenerator(next);
  }
  run(start?: number): Promise<ErrorReport[]> {
    return new Promise<ErrorReport[]>((resolve: any, reject: any) => {
      if (this.handleResult == null) reject('handleResult cannot be null');
      this.resolve = resolve;
      start = start == null ? 0 : start;
      const max =
        this.urlSource != null && this.urlSource.length - start < this.MAX ? this.urlSource.length : start + this.MAX;
      for (let index = start; index < max; index++) {
        this.curIndex = index;
        ++this.handleCount;
        const result = this.handleUrl(this.curIndex, this.getNextUrl(this.curIndex));
        if (result === 0) break;
      }
    });
  }

  createAndHandlePromise(index: number, curUrl: string) {
    this.createPromise(index, curUrl)
      .then((response: any) => {
        if (this.errorUrlCount.hasOwnProperty(curUrl)) delete this.errorUrlCount[curUrl];
        if (response.length === 0) {
          this.errorReport.push({ index, url: curUrl, reason: 'Empty result' });
        } else {
          this.handleResult(index, curUrl, response);
        }
        this.curIndex = this.curIndex + 1;
        this.handleUrl(this.curIndex, this.getNextUrl(this.curIndex));
      })
      .catch((reason: any) => {
        const count = this.errorUrlCount.hasOwnProperty(curUrl) ? this.errorUrlCount[curUrl] : 0;
        if (count < this.MAX) {
          this.errorUrlCount[curUrl] = count + 1;
          this.handleUrl(index, curUrl);
        } else {
          delete this.errorUrlCount[curUrl];
          this.errorReport.push({
            index,
            url: curUrl,
            reason: '' + reason,
          });
          this.curIndex = this.curIndex + 1;
          this.handleUrl(this.curIndex, this.getNextUrl(this.curIndex));
        }
      });
  }
  handleUrl(index: number, url: string | null): any {
    if (url == null || url.length === 0) {
      // the whole download process finished here
      --this.handleCount;
      console.log('Has ' + this.handleCount + ' downloading to finish');
      if (this.handleCount === 0) {
        this.resolve(this.errorReport);
      }
      return this.handleCount;
    }
    const gap = this.nextTry - Date.now();
    if (gap > 0) {
      console.log('wait ' + gap + 'ms to download index ' + index);
      return setTimeout(this.handleUrl.bind(this, index, url), gap);
    } else {
      return this.createAndHandlePromise(index, url);
    }
  }
  createPromise(index: number, url: string) {
    return new Promise<string>((resolve, reject) => {
      const callback = (res: http.IncomingMessage) => {
        const { statusCode } = res;
        let error: Error | null = null;
        if (statusCode === 429) {
          const retry = res.headers['retry-after'] || '';
          this.nextTry = new Date().getTime() + parseInt(retry, 10) * 1000;
          error = new Error('' + retry);
        } else if (statusCode !== 200) {
          error = new Error(
            'Request Failed.\n' + `Status Code: ${statusCode} -- ${http.STATUS_CODES[statusCode || '']}`,
          );
        }
        if (error != null) {
          // Consume response data to free up memory
          res.resume();
          reject(error);
          return;
        }

        res.setEncoding('binary');
        const rawData: Buffer[] = [];
        res.on('data', (chunk: any) => {
          rawData.push(Buffer.from(chunk, 'binary'));
        });
        res.on('end', () => {
          try {
            resolve(this.handleResponse(Buffer.concat(rawData)));
          } catch (error) {
            reject(new Error('Error happens when handling response for ' + url));
          }
        });
        res.on('error', (err: any) => {
          reject(new Error('Error during HTTP request'));
        });
      };
      let request: http.ClientRequest | null = null;
      let data = null;
      if (this.requestGenerator == null) {
        request = http.get(url, callback);
      } else {
        ({ request, data } = this.requestGenerator(http, index, url, callback));
      }
      if (request != null) {
        request = request.on('error', (e: Error) => {
          reject(e);
        });
        if (this.requestGenerator != null) {
          if (data != null && data.length > 0) request.write(data);
          request.end();
        }
      }
    });
  }
}
