import { Downloader, ErrorReport } from '../index';
it('empty downloader', () => {
  let downloader: Downloader | null = null;
  try {
    downloader = new Downloader(() => {});
    downloader.run().then((r: ErrorReport[]) => {
      expect(r.length === 0).toBeTruthy();
    });
  } catch (e) {
    expect(downloader != null).toBeFalsy();
  }
});
test('post downloader', async () => {
  let downloader: Downloader | null = null;
  const host = 'http://hy.httpcn.com/pinyin/zi/';
  try {
    downloader = new Downloader(
      (index?: number, url?: string, r?: string) => {
        expect(r != null && r.length > 0).toBeTruthy();
      },
      undefined,
      undefined,
      ['a'],
      (http: any, index: number, url: string, callback: any) => {
        const data = 'wd=' + url;
        return {
          request: http.request(
            host,
            {
              method: 'POST',
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(data),
              },
            },
            callback,
          ),
          data,
        };
      },
    );
    const r = await downloader.run();
    expect(r.length === 0).toBeTruthy();
  } catch (e) {
    expect(downloader != null).toBeFalsy();
  }
});
